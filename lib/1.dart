import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "weather",
            ),
            leading: Icon(
              Icons.add,
              color: Colors.white,
            ),
            actions: [
              Icon(
                Icons.more_vert,
                color: Colors.white,
              )
            ],
          ),
          body: Center(
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://media.istockphoto.com/id/1200224188/photo/white-clouds-and-sun-in-blue-sky.jpg?b=1&s=170667a&w=0&k=20&c=R9arjSutcN7BHEXCOvN1ihaOlTHYQCIVbKlolwv_xCE="),
                        fit: BoxFit.cover,
                      )),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 60.0),
                      child: Image.network(
                        "https://media.tenor.com/_w_ea8aRK2kAAAAi/dm4uz3-foekoe.gif",
                        height: 200,
                        width: 300,
                      ),
                    ),
                    _temp(),
                    _location(),
                    _TOday(),
                    _temprow(),
                    _tempcolum()
                  ],
                ),
                Column()
              ],
            ),
          )),
    );
  }
}

_TOday() {
  return Row(
    children: [
      Text(
        "TODAY : 27/28/2022 ",
        style: TextStyle(fontSize: 20, color: Colors.white),
      ),
    ],
  );
}

_location() {
  return Row(
    children: [
      Icon(
        Icons.location_on,
        color: Colors.white,
      ),
      SizedBox(
        width: 10,
      ),
      Text(
        "Bangsaen,Chonburi",
        style: TextStyle(fontSize: 20, color: Colors.white),
      ),
    ],
  );
}

_temp() {
  return Text(
    "29 ํ",
    style: TextStyle(
        fontSize: 80, fontWeight: FontWeight.w400, color: Colors.white),
    textAlign: TextAlign.center,
  );
}

final tims = [
  'noon 29 ํ',
  'evening 29 ํ',
  'night 29 ํ',
  '29 ํ',
  '30 ํ',
  '32 ํ',
  '28 ํ',
  '29 ํ',
  '31 ํ',
  '31 ํ',
  '30 ํ',
  '29 ํ'
];

_temprow() {
  return Container(
    height: 100,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(color: Colors.white),
        bottom: BorderSide(color: Colors.white),
      ),
    ),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: tims.length,
      itemBuilder: (context, index) {
        return Container(
          width: 50,
          child: Center(
            child: Text(
              '${tims[index]}',
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
    ),
  );
}

final num = [
  'today: sunny',
  'tomorrow : sunny',
  'Tue : sunny',
  'Wed : sunny',
  'Thu : sunny',
  'Fri : sunny',
  'Sat : sunny',
  'Sun : sunny',
  'Mon : sunny',
];
_tempcolum() {
  return Container(
    height: 310,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(color: Colors.white),
        bottom: BorderSide(color: Colors.white),
      ),
    ),
    child: ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: num.length,
      itemBuilder: (context, index) {
        return Container(
          height: 50,
          child: Center(
            child: Text(
              '${num[index]}',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      },
    ),
  );
}