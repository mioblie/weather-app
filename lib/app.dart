import 'package:flutter/material.dart';

void main() {
  runApp(const AspectRationExample());
}

class AspectRationExample extends StatelessWidget {
  const AspectRationExample({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: SizedBox(
            width: 600.0,
            child: AspectRatio(
              aspectRatio: 1.5,
              child: Container(
                color: Colors.green[200],
                child: const FlutterLogo(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
